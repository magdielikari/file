<?php

namespace common\models\query;

/**
 * This is the ActiveQuery class for [[\common\models\Population]].
 *
 * @see \common\models\Population
 */
class PopulationQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    // ASC and DESC

    /**
     *
     */
    public function current()
    {
        return $this->addOrderBy('from DESC');
    }

    /**
     * @inheritdoc
     * @return \common\models\Population[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\Population|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
