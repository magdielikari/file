<?php

namespace common\models\query;

/**
 * This is the ActiveQuery class for [[\common\models\GCR]].
 *
 * @see \common\models\GCR
 */
class GcrQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     *
     */
    public function current()
    {
        return $this->addOrderBy('from DESC');
    }

    /**
     * @inheritdoc
     * @return \common\models\GCR[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\GCR|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
