<?php

namespace common\models\query;

/**
 * This is the ActiveQuery class for [[Salud]].
 *
 * @see Salud
 */
class SaludQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return Salud[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Salud|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
