<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "metadata".
 *
 * @property string $id
 * @property string $acronym
 * @property string $name
 * @property string $definition
 * @property string $name_en
 * @property string $url 
 * @property string $created_at
 * @property string $created_by
 * @property string $updated_at
 * @property string $updated_by
 * @property string $category_id
 *
 * @property Category $category
 */
class Metadata extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'metadata';
    }

    /**
     *  Public doc   
     */    
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            BlameableBehavior::className()
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['acronym', 'name', 'definition', 'name_en', 'url', 'category_id'], 'required'],
            [['created_at', 'created_by', 'updated_at', 'updated_by', 'category_id'], 'integer'],
            [['acronym'], 'string', 'max' => 17],
            [['name', 'name_en'], 'string', 'max' => 127],
            [['definition'], 'string', 'max' => 251],
            [['url'], 'string', 'max' => 29], 
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['category_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'acronym' => Yii::t('app', 'Acronym'),
            'name' => Yii::t('app', 'Name'),
            'definition' => Yii::t('app', 'Definition'),
            'name_en' => Yii::t('app', 'Name En'),
            'url' => Yii::t('app', 'Url'), 
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'category_id' => Yii::t('app', 'Category ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\query\MetadataQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\MetadataQuery(get_called_class());
    }
}
