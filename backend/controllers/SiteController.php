<?php
namespace backend\controllers;

use Yii;
use yii\db\Query;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use common\models\Metadata;
/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => [
                            'logout', 
                            'index',
                            'socio',
                            'fiscal',
                            'informes',
                            'negocios',
                            'monetaria',
                            'geografico',
                            'comerciales',
                            'configuracion',
                            'macroeconomica',
                        ],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     *
     */
    public function actionIndex()
    {
        return $this->render('index' );
        /* 
            [
                '' => ,
            ]);
        */
    }

    /**
     *
     */
    public function actionInformes()
    {
        $meta = Metadata::find()
            ->with('category')
            ->where(['category_id' => 9])
            ->asArray()
            ->all();
        return $this->render('menu',[
                'name' => $meta[0]['category']['name'],
                'defe' => $meta[0]['category']['defenition'],
                'meta' => $meta
            ]);
    }

    /**
     *
     */
    public function actionConfiguracion()
    {
        $meta = Metadata::find()
            ->with('category')
            ->where(['category_id' => 8])
            ->asArray()
            ->all();
        return $this->render('menu',[
                'name' => $meta[0]['category']['name'],
                'defe' => $meta[0]['category']['defenition'],
                'meta' => $meta
            ]);
    }
    
    /**
     *
     */
    public function actionSocio()
    {
        $meta = Metadata::find()
            ->with('category')
            ->where(['category_id' => 7])
            ->asArray()
            ->all();
        return $this->render('menu',[
                'name' => $meta[0]['category']['name'],
                'defe' => $meta[0]['category']['defenition'],
                'meta' => $meta
            ]);
    }

    /**
     *
     */
    public function actionGeografico()
    {
        $meta = Metadata::find()
            ->with('category')
            ->where(['category_id' => 1])
            ->asArray()
            ->all();
        return $this->render('menu',[
                'name' => $meta[0]['category']['name'],
                'defe' => $meta[0]['category']['defenition'],
                'meta' => $meta
            ]);
    }


    /**
     *
     */
    public function actionFiscal()
    {
        $meta = Metadata::find()
            ->with('category')
            ->where(['category_id' => 4])
            ->asArray()
            ->all();
        return $this->render('menu',[
                'name' => $meta[0]['category']['name'],
                'defe' => $meta[0]['category']['defenition'],
                'meta' => $meta
            ]);
    }

    /**
     *
     */
    public function actionNegocios()
    {
        $meta = Metadata::find()
            ->with('category')
            ->where(['category_id' => 6])
            ->asArray()
            ->all();
        return $this->render('menu',[
                'name' => $meta[0]['category']['name'],
                'defe' => $meta[0]['category']['defenition'],
                'meta' => $meta
            ]);
    }

    /**
     *
     */
    public function actionMonetaria()
    {
        $meta = Metadata::find()
            ->with('category')
            ->where(['category_id' => 3])
            ->asArray()
            ->all();
        return $this->render('menu',[
                'name' => $meta[0]['category']['name'],
                'defe' => $meta[0]['category']['defenition'],
                'meta' => $meta
            ]);
    }

    /**
     *
     */
    public function actionComerciales()
    {
        $meta = Metadata::find()
            ->with('category')
            ->where(['category_id' => 5])
            ->asArray()
            ->all();
        return $this->render('menu',[
                'name' => $meta[0]['category']['name'],
                'defe' => $meta[0]['category']['defenition'],
                'meta' => $meta
            ]);
    }

    /**
     *
     */
    public function actionMacroeconomica()
    {
        $meta = Metadata::find()
            ->with('category')
            ->where(['category_id' => 2])
            ->asArray()
            ->all();
        return $this->render('menu',[
                'name' => $meta[0]['category']['name'],
                'defe' => $meta[0]['category']['defenition'],
                'meta' => $meta
            ]);
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}
