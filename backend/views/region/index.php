<?php

use common\components\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\RegionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Regions');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="region-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create') . ' ' . Html::title('region'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'name',
            'capital',
            'area:km2',
            'map:raw',
            // 'flag',
            // 'north',
            // 'east',
            // 'west',
            // 'south',
            // 'north_east',
            // 'north_west',
            // 'south_east',
            // 'south_west',
            'border:km',
            'coastline:km',
            // 'internet_domain',
            // 'area_code',
            // 'currency_id',
            [
                'attribute' => 'currency_id',
                'label' => Yii::t('app', 'Region Name'),
                'value' => 'currency.name'
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
