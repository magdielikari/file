<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\Currency;
/* @var $this yii\web\View */
/* @var $model common\models\Region */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="region-form">

    <?php $form = ActiveForm::begin([
    'options' => ['enctype' => 'multipart/form-data'],
]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'capital')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'area')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'map')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'image')->widget('demi\image\FormImageWidget', [
        'imageSrc' => $model->getImageSrc('medium_'),
    ]) ?>

    <?= $form->field($model, 'north')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'east')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'west')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'south')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'north_east')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'north_west')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'south_east')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'south_west')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'border')->textInput() ?>

    <?= $form->field($model, 'coastline')->textInput() ?>

    <?= $form->field($model, 'internet_domain')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'area_code')->textInput() ?>

    <?= $form->field($model, 'currency_id')->dropDownList(
        ArrayHelper::map(Currency::find()->all(),'id', 'name'),
        ['prompt'=>'Seleccione una categoria']) 
     ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
