<?php

use common\components\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\IR */

$this->title = Yii::t('app', 'Create') . ' ' . Html::title('IR');
$this->params['breadcrumbs'][] = ['label' => Html::title('IR'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ir-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
