<?php

use common\components\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\IR */

$this->title = Yii::t('app', 'Update') . ' ' . Html::title('IR');
$this->params['breadcrumbs'][] = ['label' => Html::title('IR'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="ir-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
