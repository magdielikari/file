<?php

use yii\grid\GridView;
use yii\widgets\Pjax;
use common\components\helpers\Html;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\PDSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Html::title('PD');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pd-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create') . ' ' . Html::title('PD'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'data',
                'label' => Yii::t('app', 'Data'),
                'value' => 'datas'
            ],
            [
                'attribute' => 'data_percentage',
                'value' => function($dataProvider) {
                    return Yii::$app->formatter->asPercent(($dataProvider->data_percentage/100));
                } 
            ],
            [
                'attribute' => 'data_local',
                'label' => Yii::t('app', 'Data Local'),
                'value' => 'locals'
            ],
            [
                'attribute' => 'data_local_percentage',
                'value' => function($dataProvider) {
                    return Yii::$app->formatter->asPercent(($dataProvider->data_local_percentage/100));
                } 
            ],
            'by:textShort',
            'from',
/*
            'type',
            'trend',
*/
            'comments:textShort',
            [
                'attribute' => 'region_id',
                'label' => Yii::t('app', 'Region Name'),
                'value' => 'region.name'
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
