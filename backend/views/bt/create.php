<?php

use common\components\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\BT */

$this->title = Yii::t('app', 'Create') .' '. Html::title('BT');
$this->params['breadcrumbs'][] = ['label' => Html::title('BT'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bt-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
