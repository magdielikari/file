<?php

use  common\components\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\BT */

$this->title = Yii::t('app', 'Update') . ' ' . Html::title('Bt');
$this->params['breadcrumbs'][] = ['label' => Html::title('Bt'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="bt-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
