<?php

use common\components\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\IER */

$this->title = Yii::t('app', 'Create') . ' ' . Html::title('IER');
$this->params['breadcrumbs'][] = ['label' => Html::title('IER'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ier-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
