<?php

use common\components\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\MDM */

$this->title = Yii::t('app', 'Create') . ' ' . Html::title('MDM');
$this->params['breadcrumbs'][] = ['label' => Html::title('MDM'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mdm-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
