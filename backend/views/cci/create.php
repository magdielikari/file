<?php

use common\components\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\CCI */

$this->title = Yii::t('app', 'Create') . ' ' . Html::title('Cci');
$this->params['breadcrumbs'][] = ['label' => Html::title('CCI'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cci-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
