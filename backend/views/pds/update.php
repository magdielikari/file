<?php

use common\components\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\PDS */

$this->title = Yii::t('app', 'Update') . ' ' . Html::title('PDS');
$this->params['breadcrumbs'][] = ['label' => Html::title('PDS'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="pds-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
