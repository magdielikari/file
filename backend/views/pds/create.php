<?php

use common\components\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\PDS */

$this->title = Yii::t('app', 'Create') . ' ' . Html::title('PDS');
$this->params['breadcrumbs'][] = ['label' => Html::title('PDS'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pds-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
