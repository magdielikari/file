<?php

use common\components\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\UR */

$this->title = Yii::t('app', 'Create') . ' ' . Html::title('UR');
$this->params['breadcrumbs'][] = ['label' => Html::title('UR'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ur-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
