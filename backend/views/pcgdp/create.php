<?php

use common\components\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\PCGDP */

$this->title = Yii::t('app', 'Create') . ' ' . Html::title('PC_GDP');
$this->params['breadcrumbs'][] = ['label' => Html::title('PC_GDP'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pcgdp-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
