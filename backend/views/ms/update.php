<?php

use common\components\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\MS */

$this->title = Yii::t('app', 'Update') . ' ' . Html::title('MS');
$this->params['breadcrumbs'][] = ['label' => Html::title('MS'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="ms-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
