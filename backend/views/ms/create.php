<?php

use common\components\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\MS */

$this->title = Yii::t('app', 'Create') . ' ' . Html::title('MS');
$this->params['breadcrumbs'][] = ['label' => Html::title('MS'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ms-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
