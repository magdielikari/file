<?php

use common\components\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\HDI */

$this->title = Yii::t('app', 'Create') . ' ' . Html::title('HDI');
$this->params['breadcrumbs'][] = ['label' => Html::title('HDI'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="hdi-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
