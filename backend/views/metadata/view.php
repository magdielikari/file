<?php

use common\components\helpers\Html;
use yii\widgets\DetailView;
use common\models\User;
use common\models\Category;

/* @var $this yii\web\View */
/* @var $model common\models\Metadata */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Metadatas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="metadata-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'acronym',
            'name',
            'definition',
            'name_en',
            'created_at:datetime',
            [
                'attribute'=>'created_by',
                'value'=>function($dataProvider){
                    return User::findIdentity($dataProvider->created_by)->username;
                },
            ],
            'updated_at:datetime',
            [
                'attribute'=>'updated_by',
                'value'=>function($dataProvider){
                    return User::findIdentity($dataProvider->created_by)->username;
                },
            ],
            [
                'attribute' => 'region_id',
                'label' => Yii::t('app', 'Region Name'),
                'value' => function($model){
                    return $model->category->name;
                },
            ]
        ],
    ]) ?>

</div>
