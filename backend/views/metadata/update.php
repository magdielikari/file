<?php

use common\components\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Metadata */

$this->title = Yii::t('app', 'Update') . ' ' . Html::title('Metadata');
$this->params['breadcrumbs'][] = ['label' => Html::title('Metadata'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="metadata-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
