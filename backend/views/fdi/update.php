<?php

use common\components\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\FDI */

$this->title = Yii::t('app', 'Update') . ' ' . Html::title('FDI');
$this->params['breadcrumbs'][] = ['label' => Html::title('FDI'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="fdi-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
