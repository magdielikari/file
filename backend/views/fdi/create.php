<?php

use common\components\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\FDI */

$this->title = Yii::t('app', 'Create') . ' ' . Html::title('FDI');
$this->params['breadcrumbs'][] = ['label' => Html::title('FDI'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="fdi-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
