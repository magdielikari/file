<?php

use common\components\helpers\Html;
use yii\widgets\DetailView;
use common\models\User;

/* @var $this yii\web\View */
/* @var $model common\models\MS */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Monetarias'), 'url' => ['site/monetaria']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Detalles'), 'url' => ['region/tmonetaria', 'id' => 3]];
$this->params['breadcrumbs'][] = ['label' => Html::title('ms'), 'url' => ['region/monetaria', 'id' => 3]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ms-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'descriptive',
            'by',
            'from',
            'comments',
            'created_at:datetime',
            [
                'attribute' => 'created_by',
                'value' => function($dataProvider) {
                    return User::findIdentity($dataProvider->created_by)->username;
                },
            ],
            'updated_at:datetime',
            [
                'attribute' => 'updated_by',
                'value' => function($dataProvider) {
                    return User::findIdentity($dataProvider->created_by)->username;
                },
            ],
            [
                'attribute' => 'region_id',
                'label' => Yii::t('app','Region Name'),
                'value' => function($model) {
                    return $model->region->name;
                },
            ]       
        ],
    ]) ?>

</div>
