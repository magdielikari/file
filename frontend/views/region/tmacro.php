<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use yii\helpers\ArrayHelper;
use kartik\popover\PopoverX;
use kartik\icons\Icon;
Icon::map($this);  
$meta = ArrayHelper::index($meta,'acronym');
/* @var $this yii\web\View */
/* @var $model common\models\Region */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Macroeconomicas'), 'url' => ['site/macro']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="region-view">

    <h1><?= Html::encode($this->title) ?></h1>
    <h3><a class ='btn btn-success' href = <?= Url::to(['region/macro', 'id' => $id]) ?> >Ver detalles</a></h3>
    
    <div class="row">
        <div class="col-md-11">
            <h3><?= $meta['gdp']['name'] ?></h3>
        </div>
        <div class="col-md-1">
            <?= PopoverX::widget([
                'header' => $meta['gdp']['name'],
                'type' => PopoverX::TYPE_INFO,
                'placement' => PopoverX::ALIGN_LEFT,
                'content' => $meta['gdp']['definition'],
                'toggleButton' => ['label'=>'Info', 'class'=>'btn btn-info'],
            ]); ?>
        </div>
    </div>

    <div class="row">
        <div class="alert alert-info" role="alert">
            <?= $model->tgdp ?>
            <a href = <?= isset($model->gdp) ? Url::to(['region/gdp', 'id' => $model->gdp->id]) : '' ?> >
                <i class="glyphicon glyphicon-info-sign"></i>
            </a>
            <br>
            <em><?= $model->gdp ? $model->gdp->commentss : '' ?></em> 
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-11">
            <h3><?= $meta['pc_gdp']['name'] ?></h3>
        </div>
        <div class="col-md-1">
            <?= PopoverX::widget([
                'header' => $meta['pc_gdp']['name'],
                'type' => PopoverX::TYPE_INFO,
                'placement' => PopoverX::ALIGN_LEFT,
                'content' => $meta['pc_gdp']['definition'],
                'toggleButton' => ['label'=>'Info', 'class'=>'btn btn-info'],
            ]); ?>
        </div>
    </div>

    <div class="row">
        <div class="alert alert-info" role="alert">
            <?= $model->tpcgdp ?>
            <a href = <?= isset($model->pcGdp) ? Url::to(['region/pcgdp', 'id' => $model->pcGdp->id]) : '' ?> >
                <i class="glyphicon glyphicon-info-sign"></i>
            </a>
            <br>
            <em><?= $model->pcGdp ? $model->pcGdp->commentss : '' ?></em> 
        </div>
    </div>

    <div class="row">
        <div class="col-md-11">
            <h3><?= $meta['pd']['name'] ?></h3>
        </div>
        <div class="col-md-1">
            <?= PopoverX::widget([
                'header' => $meta['pd']['name'],
                'type' => PopoverX::TYPE_INFO,
                'placement' => PopoverX::ALIGN_LEFT,
                'content' => $meta['pd']['definition'],
                'toggleButton' => ['label'=>'Info', 'class'=>'btn btn-info'],
            ]); ?>
        </div>
    </div>


    <div class="row">
        <div class="alert alert-info" role="alert">
            <?= $model->tpd ?>
            <a href = <?= isset($model->pd) ? Url::to(['region/pd', 'id' => $model->pd->id]) : '' ?> >
                <i class="glyphicon glyphicon-info-sign"></i>
            </a>
            <br>
            <em><?= $model->pd ? $model->pd->commentss : '' ?></em>     
        </div>
    </div>

    <div class="row">
        <div class="col-md-11">
            <h3><?= $meta['pc_pd']['name'] ?></h3>
        </div>
        <div class="col-md-1">
            <?= PopoverX::widget([
                'header' => $meta['pc_pd']['name'],
                'type' => PopoverX::TYPE_INFO,
                'placement' => PopoverX::ALIGN_LEFT,
                'content' => $meta['pc_pd']['definition'],
                'toggleButton' => ['label'=>'Info', 'class'=>'btn btn-info'],
            ]); ?>
        </div>
    </div>

    <div class="row">
        <div class="alert alert-info" role="alert">
            <?= $model->tpcpd ?>
            <a href = <?= isset($model->pcPd) ? Url::to(['region/pcpd', 'id' => $model->pcPd->id]) : '' ?> >
                <i class="glyphicon glyphicon-info-sign"></i>
            </a>
            <br>
            <em><?= $model->pcPd ? $model->pcPd->commentss : '' ?></em>     
        </div> 
    </div>

    <div class="row">
        <div class="col-md-11">
            <h3><?= $meta['ir']['name'] ?></h3>
        </div>
        <div class="col-md-1">
            <?= PopoverX::widget([
                'header' => $meta['ir']['name'],
                'type' => PopoverX::TYPE_INFO,
                'placement' => PopoverX::ALIGN_LEFT,
                'content' => $meta['ir']['definition'],
                'toggleButton' => ['label'=>'Info', 'class'=>'btn btn-info'],
            ]); ?>
        </div>
    </div>

    <div class="row">
        <div class="alert alert-info" role="alert">
            <?= $model->tir ?>
            <a href = <?= isset($model->ir) ? Url::to(['region/ir', 'id' => $model->ir->id]) : '' ?> >
                <i class="glyphicon glyphicon-info-sign"></i>
            </a>
            <br>
            <em><?= $model->ir ? $model->ir->commentss : '' ?></em> 
        </div>
    </div>

    <div class="row">
        <div class="col-md-11">
            <h3><?= $meta['ur']['name'] ?></h3>
        </div>
        <div class="col-md-1">
            <?= PopoverX::widget([
                'header' => $meta['ur']['name'],
                'type' => PopoverX::TYPE_INFO,
                'placement' => PopoverX::ALIGN_LEFT,
                'content' => $meta['ur']['definition'],
                'toggleButton' => ['label'=>'Info', 'class'=>'btn btn-info'],
            ]); ?>
        </div>
    </div>

    <div class="row">
        <div class="alert alert-info" role="alert">
            <?= $model->tur ?>
            <a href = <?= isset($model->ur) ? Url::to(['region/ur', 'id' => $model->ur->id]) : '' ?> >
                <i class="glyphicon glyphicon-info-sign"></i>
            </a>
            <br>
            <em><?= $model->ur ? $model->ur->commentss : '' ?></em>     
        </div>
    </div>

    <div class="row">
        <div class="col-md-11">
            <h3><?= $meta['inr']['name'] ?></h3>
        </div>
        <div class="col-md-1">
            <?= PopoverX::widget([
                'header' => $meta['inr']['name'],
                'type' => PopoverX::TYPE_INFO,
                'placement' => PopoverX::ALIGN_LEFT,
                'content' => $meta['inr']['definition'],
                'toggleButton' => ['label'=>'Info', 'class'=>'btn btn-info'],
            ]); ?>
        </div>
    </div>

    <div class="row">
        <div class="alert alert-info" role="alert">
            <?= $model->tinr ?>    
            <a href = <?= isset($model->inr) ? Url::to(['region/inr', 'id' => $model->inr->id]) : '' ?> >
                <i class="glyphicon glyphicon-info-sign"></i>
            </a>
            <br>
            <em><?= $model->inr ? $model->inr->commentss : '' ?></em> 
        </div>
    </div>

    <div class="row">
        <div class="col-md-11">
            <h3><?= $meta['bt']['name'] ?></h3>
        </div>
        <div class="col-md-1">
            <?= PopoverX::widget([
                'header' => $meta['bt']['name'],
                'type' => PopoverX::TYPE_INFO,
                'placement' => PopoverX::ALIGN_LEFT,
                'content' => $meta['bt']['definition'],
                'toggleButton' => ['label'=>'Info', 'class'=>'btn btn-info'],
            ]); ?>
        </div>
    </div>

    <div class="row">
        <div class="alert alert-info" role="alert">
            <?= $model->tbt ?>
            <a href = <?= isset($model->bt) ? Url::to(['region/bt', 'id' => $model->bt->id]) : '' ?> >
                <i class="glyphicon glyphicon-info-sign"></i>
            </a>
            <br>
            <em><?= $model->bt ? $model->bt->commentss : '' ?></em>         
        </div> 
    </div>

    <div class="row">
        <div class="col-md-11">
            <h3><?= $meta['fd']['name'] ?></h3>
        </div>
        <div class="col-md-1">
            <?= PopoverX::widget([
                'header' => $meta['fd']['name'],
                'type' => PopoverX::TYPE_INFO,
                'placement' => PopoverX::ALIGN_LEFT,
                'content' => $meta['fd']['definition'],
                'toggleButton' => ['label'=>'Info', 'class'=>'btn btn-info'],
            ]); ?>
        </div>
    </div>

    <div class="row">
        <div class="alert alert-info" role="alert">
            <?= $model->tfd ?>
            <a href = <?= isset($model->fd) ? Url::to(['region/fd', 'id' => $model->fd->id]) : '' ?> >
                <i class="glyphicon glyphicon-info-sign"></i>
            </a>
            <br>
            <em><?= $model->fd ? $model->fd->commentss : '' ?></em>  
        </div>
    </div>

</div>
