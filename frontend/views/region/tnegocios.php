<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use yii\helpers\ArrayHelper;
use kartik\popover\PopoverX;
use kartik\icons\Icon;
Icon::map($this);  
$meta = ArrayHelper::index($meta,'acronym');

/* @var $this yii\web\View */
/* @var $model common\models\Region */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Negocios'), 'url' => ['site/negocios']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="region-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <h3><a class ='btn btn-success' href = <?= Url::to(['region/negocios', 'id' => $id]) ?> >Ver detalles</a></h3>

    <div class="row">
        <div class="col-md-11">
            <h3><?= $meta['cci']['name'] ?></h3>
        </div>
        <div class="col-md-1">
            <?= PopoverX::widget([
                'header' => $meta['cci']['name'],
                'type' => PopoverX::TYPE_INFO,
                'placement' => PopoverX::ALIGN_LEFT,
                'content' => $meta['cci']['definition'],
                'toggleButton' => ['label'=>'Info', 'class'=>'btn btn-info'],
            ]); ?>
        </div>
    </div>

    <div class="row">
        <div class="alert alert-info" role="alert">
            <?= $model->tcci ?>
            <a href = <?= isset($model->cci) ? Url::to(['region/cci', 'id' => $model->cci->id]) : '' ?> >
                <i class="glyphicon glyphicon-info-sign"></i>
            </a>
            <br>
            <em><?= $model->cci ? $model->cci->commentss : '' ?></em>         
        </div>
    </div>

    <div class="row">
        <div class="col-md-11">
            <h3><?= $meta['ici']['name'] ?></h3>
        </div>
        <div class="col-md-1">
            <?= PopoverX::widget([
                'header' => $meta['ici']['name'],
                'type' => PopoverX::TYPE_INFO,
                'placement' => PopoverX::ALIGN_LEFT,
                'content' => $meta['ici']['definition'],
                'toggleButton' => ['label'=>'Info', 'class'=>'btn btn-info'],
            ]); ?>
        </div>
    </div>

    <div class="row">
        <div class="alert alert-info" role="alert">
            <?= $model->tici ?>
            <a href = <?= isset($model->ici) ? Url::to(['region/ici', 'id' => $model->ici->id]) : '' ?> >
                <i class="glyphicon glyphicon-info-sign"></i>
            </a>
            <br>
            <em><?= $model->ici ? $model->ici->commentss : '' ?></em>  
        </div>
    </div>

    <div class="row">
        <div class="col-md-11">
            <h3><?= $meta['gcr']['name'] ?></h3>
        </div>
        <div class="col-md-1">
            <?= PopoverX::widget([
                'header' => $meta['gcr']['name'],
                'type' => PopoverX::TYPE_INFO,
                'placement' => PopoverX::ALIGN_LEFT,
                'content' => $meta['gcr']['definition'],
                'toggleButton' => ['label'=>'Info', 'class'=>'btn btn-info'],
            ]); ?>
        </div>
    </div>

    <div class="row">
        <div class="alert alert-info" role="alert">
            <?= $model->tgcr ?>
            <a href = <?= isset($model->gcr) ? Url::to(['region/gcr', 'id' => $model->gcr->id]) : '' ?> >
                <i class="glyphicon glyphicon-info-sign"></i>
            </a>
            <br>
            <em><?= $model->gcr ? $model->gcr->commentss : '' ?></em>    
        </div>  
    </div>

    <div class="row">
        <div class="col-md-11">
            <h3><?= $meta['mpfdb']['name'] ?></h3>
        </div>
        <div class="col-md-1">
            <?= PopoverX::widget([
                'header' => $meta['mpfdb']['name'],
                'type' => PopoverX::TYPE_INFO,
                'placement' => PopoverX::ALIGN_LEFT,
                'content' => $meta['mpfdb']['definition'],
                'toggleButton' => ['label'=>'Info', 'class'=>'btn btn-info'],
            ]); ?>
        </div>
    </div>

    <div class="row">
        <div class="alert alert-info" role="alert">
            <?= $model->tmpfdb ?>
            <a href = <?= isset($model->mpfdb) ? Url::to(['region/mpfdb', 'id' => $model->mpfdb->id]) : '' ?> >
                <i class="glyphicon glyphicon-info-sign"></i>
            </a>
            <br>
            <em><?= $model->mpfdb ? $model->mpfdb->commentss : '' ?></em>  
        </div>
    </div>

    <div class="row">
        <div class="col-md-11">
            <h3><?= $meta['dbi']['name'] ?></h3>
        </div>
        <div class="col-md-1">
            <?= PopoverX::widget([
                'header' => $meta['dbi']['name'],
                'type' => PopoverX::TYPE_INFO,
                'placement' => PopoverX::ALIGN_LEFT,
                'content' => $meta['dbi']['definition'],
                'toggleButton' => ['label'=>'Info', 'class'=>'btn btn-info'],
            ]); ?>
        </div>
    </div>

    <div class="row">
        <div class="alert alert-info" role="alert">
            <?= $model->tdbi ?>
            <a href = <?= isset($model->dbi) ? Url::to(['region/dbi', 'id' => $model->dbi->id]) : '' ?> >
                <i class="glyphicon glyphicon-info-sign"></i>
            </a> 
            <br>
            <em><?= $model->dbi ? $model->dbi->commentss : '' ?></em> 
        </div>
    </div>

    <div class="row">
        <div class="col-md-11">
            <h3><?= $meta['grdi']['name'] ?></h3>
        </div>
        <div class="col-md-1">
            <?= PopoverX::widget([
                'header' => $meta['grdi']['name'],
                'type' => PopoverX::TYPE_INFO,
                'placement' => PopoverX::ALIGN_LEFT,
                'content' => $meta['grdi']['definition'],
                'toggleButton' => ['label'=>'Info', 'class'=>'btn btn-info'],
            ]); ?>
        </div>
    </div>

    <div class="row">
        <div class="alert alert-info" role="alert">
            <?= $model->tgrdi ?>
            <a href = <?= isset($model->grdi) ? Url::to(['region/tgrdi', 'id' => $model->grdi->id]) : '' ?> >
                <i class="glyphicon glyphicon-info-sign"></i>
            </a>
            <br>
            <em><?= $model->grdi ? $model->grdi->commentss : '' ?></em> 
        </div>
    </div>
</div>
