<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\VarDumper;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model common\models\Region */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Geografico'), 'url' => ['site/geografico']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="region-view">

    <h1><?= Html::encode($this->title) ?></h1>
    
    <div class="row">
        <div class="col-md-6">
            
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'capital',
                'internet_domain',
                'currency.name',
                [
                    'attribute' => 'img',
                    'format' => 'html',
                    'label' => 'flat',
                    'value' => function ($dataProvider) {
                        return Html::img($dataProvider->imageSrc);
                    },
                ],
            ],
        ]) ?>
        </div>

        <div class="col-md-6">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'area_code',
                'area:km2',
                'border:km',
                'coastline:km',
                'map:raw',
            ],
        ]) ?>
        </div>   
    </div>
    
    <h2><?= Yii::t('app', 'Population')?></h2>
    <?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
 
            'data:hab',
            'by',
            'from',
            'type',
            'trend',       
              
        ],
    ]); ?>
    <?php Pjax::end(); ?></div>      
        
    <h2><?= Yii::t('app', 'Limits') ?></h2>
    
    <div class="row">
        <div class="col-md-6">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'north',
                    'east',
                    'west',
                    'south',
                ],
            ]) ?>
        </div>    

        <div class="col-md-6">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'north_east',
                    'north_west',
                    'south_east',
                    'south_west',
                ],
            ]) ?>
        </div>
    </div>
</div>
