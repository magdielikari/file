<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;
use yii\widgets\Pjax;
use kartik\popover\PopoverX;
$meta = ArrayHelper::index($meta,'acronym');

/* @var $this yii\web\View */
/* @var $model common\models\Region */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Comercial'), 'url' => ['site/comercial']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Detalles'), 'url' => ['region/tcomercial', 'id' => $model->id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="region-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="row">
        <div class="col-md-11">
            <h3><?= $meta['mta']['name'] ?></h3>
        </div>
        <div class="col-md-1">
            <?= PopoverX::widget([
                'header' => $meta['mta']['name'],
                'type' => PopoverX::TYPE_INFO,
                'placement' => PopoverX::ALIGN_LEFT,
                'content' => $meta['mta']['definition'],
                'toggleButton' => ['label'=>'Info', 'class'=>'btn btn-info'],
            ]); ?>
        </div>
    </div>
    <br>

    <?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataMta,
        'columns' => [
            
            'descriptive:textShort',
            'to',
            'region.name',
            'created_at:datetime',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{info}',
                'buttons' => [
                    'info' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-info-sign"></span>', $url, [
                                    'title' => Yii::t('app', 'Info'),
                        ]);
                    }
                ],
                'urlCreator' => function ($action, $model, $key, $index) {
                    if ($action === 'info') {
                        return  Url::toRoute(['region/mta', 'id' => $model->id]);
                    }
                }
            ],         
        ],
    ]); ?>
    <?php Pjax::end(); ?>

    <div class="row">
        <div class="col-md-11">
            <h3><?= $meta['mdm']['name'] ?></h3>
        </div>
        <div class="col-md-1">
            <?= PopoverX::widget([
                'header' => $meta['mdm']['name'],
                'type' => PopoverX::TYPE_INFO,
                'placement' => PopoverX::ALIGN_LEFT,
                'content' => $meta['mdm']['definition'],
                'toggleButton' => ['label'=>'Info', 'class'=>'btn btn-info'],
            ]); ?>
        </div>
    </div>
    <br>

    <?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataMdm,
        'columns' => [
            
            'name',
            'data_percentage:percent',
            'by',
            'from',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{info}',
                'buttons' => [
                    'info' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-info-sign"></span>', $url, [
                                    'title' => Yii::t('app', 'Info'),
                        ]);
                    }
                ],
                'urlCreator' => function ($action, $model, $key, $index) {
                    if ($action === 'info') {
                        return  Url::toRoute(['region/mdm', 'id' => $model->id]);
                    }
                }
            ],         
        ],
    ]); ?>
    <?php Pjax::end(); ?>   

    <div class="row">
        <div class="col-md-11">
            <h3><?= $meta['msm']['name'] ?></h3>
        </div>
        <div class="col-md-1">
            <?= PopoverX::widget([
                'header' => $meta['msm']['name'],
                'type' => PopoverX::TYPE_INFO,
                'placement' => PopoverX::ALIGN_LEFT,
                'content' => $meta['msm']['definition'],
                'toggleButton' => ['label'=>'Info', 'class'=>'btn btn-info'],
            ]); ?>
        </div>
    </div>
    <br>

    <?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataMsm,
        'columns' => [
            
            'name',
            'data_percentage',
            'by',
            'from',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{info}',
                'buttons' => [
                    'info' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-info-sign"></span>', $url, [
                                    'title' => Yii::t('app', 'Info'),
                        ]);
                    }
                ],
                'urlCreator' => function ($action, $model, $key, $index) {
                    if ($action === 'info') {
                        return  Url::toRoute(['region/msm', 'id' => $model->id]);
                    }
                }
            ],         
        ],
    ]); ?>
    <?php Pjax::end(); ?>   

    <div class="row">
        <div class="col-md-11">
            <h3><?= $meta['fdi']['name'] ?></h3>
        </div>
        <div class="col-md-1">
            <?= PopoverX::widget([
                'header' => $meta['fdi']['name'],
                'type' => PopoverX::TYPE_INFO,
                'placement' => PopoverX::ALIGN_LEFT,
                'content' => $meta['fdi']['definition'],
                'toggleButton' => ['label'=>'Info', 'class'=>'btn btn-info'],
            ]); ?>
        </div>
    </div>
    <br>

    <?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataFdi,
        'columns' => [
            
            'data',
            'data_unit',
            'by',
            'from',    
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{info}',
                'buttons' => [
                    'info' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-info-sign"></span>', $url, [
                                    'title' => Yii::t('app', 'Info'),
                        ]);
                    }
                ],
                'urlCreator' => function ($action, $model, $key, $index) {
                    if ($action === 'info') {
                        return  Url::toRoute(['region/fdi', 'id' => $model->id]);
                    }
                }
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>

</div>
