<?php

use common\components\helpers\Html;
use yii\widgets\DetailView;
use common\models\User;

/* @var $this yii\web\View */
/* @var $model common\models\DI */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Negocios'), 'url' => ['site/negocios']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Detalles'), 'url' => ['region/tnegocios', 'id' => 7]];
$this->params['breadcrumbs'][] = ['label' => Html::title('di'), 'url' => ['region/negocios', 'id' => 7]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="di-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'data_rank',
            'data_position',
            'data_rank_score',
            'data_position_score',
            'by',
            'from',
            'type',
            'trend',
            'created_at:datetime',
            [
                'attribute' => 'created_by',
                'value' => function($dataProvider) {
                    return User::findIdentity($dataProvider->created_by)->username;
                },
            ],
            'updated_at:datetime',
            [
                'attribute' => 'updated_by',
                'value' => function($dataProvider) {
                    return User::findIdentity($dataProvider->created_by)->username;
                },
            ],
            [
                'attribute' => 'region',
                'value' => function($model) {
                    return $model->region->name;
                },
            ]
        ],
    ]) ?>

</div>
