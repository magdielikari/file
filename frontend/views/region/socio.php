<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;
use yii\widgets\Pjax;
use kartik\popover\PopoverX;
$meta = ArrayHelper::index($meta,'acronym');

/* @var $this yii\web\View */
/* @var $model common\models\Region */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Sociopolíticos'), 'url' => ['site/socio']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Detalles'), 'url' => ['region/tsocio', 'id' => $model->id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="region-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="row">
        <div class="col-md-11">
            <h3><?= $meta['hdi']['name'] ?></h3>
        </div>
        <div class="col-md-1">
            <?= PopoverX::widget([
                'header' => $meta['hdi']['name'],
                'type' => PopoverX::TYPE_INFO,
                'placement' => PopoverX::ALIGN_LEFT,
                'content' => $meta['hdi']['definition'],
                'toggleButton' => ['label'=>'Info', 'class'=>'btn btn-info'],
            ]); ?>
        </div>
    </div>
    <br>

    <?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataHdi,
        'columns' => [
            
            'data',
            'total_quantity', 
            'data_position', 
            'percentile',
            'by',
            'from',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{info}',
                'buttons' => [
                    'info' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-info-sign"></span>', $url, [
                                    'title' => Yii::t('app', 'Info'),
                        ]);
                    }
                ],
                'urlCreator' => function ($action, $model, $key, $index) {
                    if ($action === 'info') {
                        return  Url::toRoute(['region/hdi', 'id' => $model->id]);
                    }
                }
            ],         
        ],
    ]); ?>
    <?php Pjax::end(); ?>

    <div class="row">
        <div class="col-md-11">
            <h3><?= $meta['fwi']['name'] ?></h3>
        </div>
        <div class="col-md-1">
            <?= PopoverX::widget([
                'header' => $meta['fwi']['name'],
                'type' => PopoverX::TYPE_INFO,
                'placement' => PopoverX::ALIGN_LEFT,
                'content' => $meta['fwi']['definition'],
                'toggleButton' => ['label'=>'Info', 'class'=>'btn btn-info'],
            ]); ?>
        </div>
    </div>
    <br>

    <?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataFwi,
        'columns' => [
            
            'data_rank',
            'data_position',
            'total_quantity',
            'status',
            'by',
            'from',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{info}',
                'buttons' => [
                    'info' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-info-sign"></span>', $url, [
                                    'title' => Yii::t('app', 'Info'),
                        ]);
                    }
                ],
                'urlCreator' => function ($action, $model, $key, $index) {
                    if ($action === 'info') {
                        return  Url::toRoute(['region/fwi', 'id' => $model->id]);
                    }
                }
            ],         
        ],
    ]); ?>
    <?php Pjax::end(); ?>   

    <div class="row">
        <div class="col-md-11">
            <h3><?= $meta['gpi']['name'] ?></h3>
        </div>
        <div class="col-md-1">
            <?= PopoverX::widget([
                'header' => $meta['gpi']['name'],
                'type' => PopoverX::TYPE_INFO,
                'placement' => PopoverX::ALIGN_LEFT,
                'content' => $meta['gpi']['definition'],
                'toggleButton' => ['label'=>'Info', 'class'=>'btn btn-info'],
            ]); ?>
        </div>
    </div>
    <br>

    <?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataGpi,
        'columns' => [
            
            'data_rank',
            'data_position',
            'score', 
            'by',
            'from',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{info}',
                'buttons' => [
                    'info' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-info-sign"></span>', $url, [
                                    'title' => Yii::t('app', 'Info'),
                        ]);
                    }
                ],
                'urlCreator' => function ($action, $model, $key, $index) {
                    if ($action === 'info') {
                        return  Url::toRoute(['region/gpi', 'id' => $model->id]);
                    }
                }
            ],         
        ],
    ]); ?>
    <?php Pjax::end(); ?>   

    <div class="row">
        <div class="col-md-11">
            <h3><?= $meta['whr']['name'] ?></h3>
        </div>
        <div class="col-md-1">
            <?= PopoverX::widget([
                'header' => $meta['whr']['name'],
                'type' => PopoverX::TYPE_INFO,
                'placement' => PopoverX::ALIGN_LEFT,
                'content' => $meta['whr']['definition'],
                'toggleButton' => ['label'=>'Info', 'class'=>'btn btn-info'],
            ]); ?>
        </div>
    </div>
    <br>

    <?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataWhr,
        'columns' => [
            
            'data_rank',
            'data_position',
            'score', 
            'by',
            'from',   
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{info}',
                'buttons' => [
                    'info' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-info-sign"></span>', $url, [
                                    'title' => Yii::t('app', 'Info'),
                        ]);
                    }
                ],
                'urlCreator' => function ($action, $model, $key, $index) {
                    if ($action === 'info') {
                        return  Url::toRoute(['region/whr', 'id' => $model->id]);
                    }
                }
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>

    <div class="row">
        <div class="col-md-11">
            <h3><?= $meta['di']['name'] ?></h3>
        </div>
        <div class="col-md-1">
            <?= PopoverX::widget([
                'header' => $meta['di']['name'],
                'type' => PopoverX::TYPE_INFO,
                'placement' => PopoverX::ALIGN_LEFT,
                'content' => $meta['di']['definition'],
                'toggleButton' => ['label'=>'Info', 'class'=>'btn btn-info'],
            ]); ?>
        </div>
    </div>
    <br>

    <?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataDi,
        'columns' => [
            
            'data_rank',
            'data_position',
            'data_rank_score',
            'data_position_score',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{info}',
                'buttons' => [
                    'info' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-info-sign"></span>', $url, [
                                    'title' => Yii::t('app', 'Info'),
                        ]);
                    }
                ],
                'urlCreator' => function ($action, $model, $key, $index) {
                    if ($action === 'info') {
                        return  Url::toRoute(['region/di', 'id' => $model->id]);
                    }
                }
            ],         
        ],
    ]); ?>
    <?php Pjax::end(); ?>   

    <div class="row">
        <div class="col-md-11">
            <h3><?= $meta['pci']['name'] ?></h3>
        </div>
        <div class="col-md-1">
            <?= PopoverX::widget([
                'header' => $meta['pci']['name'],
                'type' => PopoverX::TYPE_INFO,
                'placement' => PopoverX::ALIGN_LEFT,
                'content' => $meta['pci']['definition'],
                'toggleButton' => ['label'=>'Info', 'class'=>'btn btn-info'],
            ]); ?>
        </div>
    </div>
    <br>

    <?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataPci,
        'columns' => [
            
            'data_rank',
            'data_position',
            'data_rank_score',
            'data_position_score', 
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{info}',
                'buttons' => [
                    'info' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-info-sign"></span>', $url, [
                                    'title' => Yii::t('app', 'Info'),
                        ]);
                    }
                ],
                'urlCreator' => function ($action, $model, $key, $index) {
                    if ($action === 'info') {
                        return  Url::toRoute(['region/pci', 'id' => $model->id]);
                    }
                }
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>

</div>
