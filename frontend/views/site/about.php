<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use common\models\Region;
use yii\helpers\VarDumper;
$this->title = 'About';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>
	<?php 
		$s = Region::find()->with('populations')->where(['id'=>1])->one();
		VarDumper::dump($s->populations);
	?>
    <p>This is the About page. You may modify the following file to customize its content:</p>
	
	<?php 
		$a = 'This is the About page. You may modify the following file to customize its content';
		$b = strlen($a);
		//substr_count($a);
		$c = substr($a, 0, 17);
		$d = $c . '...';
		var_dump($d);
	?>
    <code><?= __FILE__ ?></code>
</div>
