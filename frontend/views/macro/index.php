<?php

use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
use common\models\User;
use common\components\helpers\Html;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\MacroSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Html::title('Macro');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="macro-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create') . ' ' . Html::title('Macro'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'rowOptions' => function($model){
            if($model->error == 0){
                return ['class' => 'success'];
            }else{
                return ['class' => 'danger'];
            }
        },
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'name',
            'size:shortSize',
            'created_at:datetime',
            [
                'attribute'=>'created_by',
                'value'=>function($dataProvider){
                    return User::findIdentity($dataProvider->created_by)->username;
                },
            ],
            [
                'attribute' => 'region_id',
                'label' => Yii::t('app', 'Region Name'),
                'value' => 'region.name'
            ],

            ['class' => 'yii\grid\ActionColumn',
                'template'=>'{view}',
                'urlCreator' => function ($action, $model, $key, $index) {
                    if ($action === 'view') {
                        return Url::to('@web/admin/' . $model->path);
                    }
                }
            ]
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
