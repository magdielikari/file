<?php

use common\components\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Macro */

$this->title = Yii::t('app', 'Create') . ' ' . Html::title('Macro');
$this->params['breadcrumbs'][] = ['label' => Html::title('Macro'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="macro-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
