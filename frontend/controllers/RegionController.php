<?php

namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;

use common\models\Region;
use frontend\models\search\RegionSearch;

use common\models\Metadata;
use frontend\models\search\PopulationSearch;

use common\models\Str;
use frontend\models\search\STRSearch;

use common\models\Rvt;
use frontend\models\search\RVTSearch;

use common\models\Rit;
use frontend\models\search\RITSearch;

use common\models\Etrcp;
use frontend\models\search\ETRCPSearch;

use common\models\Ms;
use frontend\models\search\MSSearch;

use common\models\Ml;
use frontend\models\search\MLSearch;

use common\models\Mw;
use frontend\models\search\MWSearch;

use common\models\Trm;
use frontend\models\search\TRMSearch;

use common\models\Ier;
use frontend\models\search\IERSearch;

use common\models\Gdp;
use frontend\models\search\GDPSearch;

use common\models\Pcgdp;
use frontend\models\search\PCGDPSearch;

use common\models\Pd;
use frontend\models\search\PDSearch;

use common\models\Pcpd;
use frontend\models\search\PCPDSearch;

use common\models\Ir;
use frontend\models\search\IRSearch;

use common\models\Ur;
use frontend\models\search\URSearch;

use common\models\Inr;
use frontend\models\search\INRSearch;

use common\models\Bt;
use frontend\models\search\BTSearch;

use common\models\Fd;
use frontend\models\search\FDSearch;

use common\models\Mta;
use frontend\models\search\MTASearch;

use common\models\Mdm;
use frontend\models\search\MDMSearch;

use common\models\Msm;
use frontend\models\search\MSMSearch;

use common\models\Fdi;
use frontend\models\search\FDISearch;

use common\models\Cci;
use frontend\models\search\CCISearch;

use common\models\Ici;
use frontend\models\search\ICISearch;

use common\models\Gcr;
use frontend\models\search\GCRSearch;

use common\models\Mpfdb;
use frontend\models\search\MPFDBSearch;

use common\models\Dbi;
use frontend\models\search\DBISearch;

use common\models\Grdi;
use frontend\models\search\GRDISearch;

use common\models\Hdi;
use frontend\models\search\HDISearch;

use common\models\Fwi;
use frontend\models\search\FWISearch;

use common\models\Gpi;
use frontend\models\search\GPISearch;

use common\models\Whr;
use frontend\models\search\WHRSearch;

use common\models\Di;
use frontend\models\search\DISearch;

use common\models\Pci;
use frontend\models\search\PCISearch;

/**
 * RegionController implements the CRUD actions for Region model.
 */
class RegionController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Region models.
     * @return mixed
     */
        //$this->layout = false;
    public function actionIndex()
    {
        $searchModel = new RegionSearch();
        $query = Yii::$app->request->queryParams;
        $dataProvider = $searchModel->search($query);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Region model.
     * @param string $id
     * @return mixed
     */
    public function actionGeografico($id)
    {
        $searchModel = new PopulationSearch();
        $query = [
                'PopulationSearch' => [
                    'region_id' => $id
                    ] 
                ];
        $dataProvider = $searchModel->search($query);
        return $this->render('geografico', [
            'model'        => $this->findModel($id),
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * 
     */
    public function actionTmacro($id)
    {
        $meta = Metadata::find()
            ->with('category')
            ->where(['category_id' => 2])
            ->asArray()
            ->all();
        return $this->render('tmacro', [
            'id' => $id,
            'meta' => $meta,
            'model' => $this->findModel($id)
        ]);  
    }


        /**
     * 
     */
    public function actionTmonetaria($id)
    {
        $meta = Metadata::find()
            ->with('category')
            ->where(['category_id' => 3])
            ->asArray()
            ->all();
        return $this->render('tmonetaria', [
            'id' => $id,
            'meta' => $meta,
            'model' => $this->findModel($id)
        ]);  
    }

        /**
     * 
     */
    public function actionTfiscal($id)
    {
        $meta = Metadata::find()
            ->with('category')
            ->where(['category_id' => 4])
            ->asArray()
            ->all();
        return $this->render('tfiscal', [
            'id' => $id,
            'meta' => $meta,
            'model' => $this->findModel($id)
        ]);  
    }

        /**
     * 
     */
    public function actionTcomercial($id)
    {
        $meta = Metadata::find()
            ->with('category')
            ->where(['category_id' => 5])
            ->asArray()
            ->all();
        return $this->render('tcomercial', [
            'id' => $id,
            'meta' => $meta,
            'model' => $this->findModel($id)
        ]);  
    }

        /**
     * 
     */
    public function actionTnegocios($id)
    {
        $meta = Metadata::find()
            ->with('category')
            ->where(['category_id' => 6])
            ->asArray()
            ->all();
        return $this->render('tnegocios', [
            'id' => $id,
            'meta' => $meta,
            'model' => $this->findModel($id)
        ]);  
    }

        /**
     * 
     */
    public function actionTsocio($id)
    {
        $meta = Metadata::find()
            ->with('category')
            ->where(['category_id' => 7])
            ->asArray()
            ->all();
        return $this->render('tsocio', [
            'id' => $id,
            'meta' => $meta,
            'model' => $this->findModel($id)
        ]);  
    }

    /**
     * Displays a single Region model.
     * @param string $id
     * @return mixed
     */
    public function actionMacro($id)
    {
        $meta = Metadata::find()
            ->with('category')
            ->where(['category_id' => 2])
            ->asArray()
            ->all();
        
        $searchGdp   = new GDPSearch();
        $searchPcgdp = new PCGDPSearch();
        $searchPd    = new PDSearch();
        $searchPcpd  = new PCPDSearch();
        $searchIr    = new IRSearch();
        $searchUr    = new URSearch();
        $searchInr   = new INRSearch();
        $searchBt    = new BTSearch();
        $searchFd    = new FDSearch();

        $gdp = [
            'GDPSearch' => [
                'region_id' => $id
            ] 
        ];
        $pcgdp = [
            'PCGDPSearch' => [
                'region_id' => $id
            ] 
        ];
        $pd = [
            'PDSearch' => [
                'region_id' => $id
            ] 
        ];
        $pcpd = [
            'PCPDSearch' => [
                'region_id' => $id
            ] 
        ];
        $ir = [
            'IRSearch' => [
                'region_id' => $id
            ] 
        ];
        $ur = [
            'URSearch' => [
                'region_id' => $id
            ] 
        ];
        $inr = [
            'INRSearch' => [
                'region_id' => $id
            ] 
        ];
        $bt = [
            'BTSearch' => [
                'region_id' => $id
            ] 
        ];
        $fd = [
            'FDSearch' => [
                'region_id' => $id
            ] 
        ];

        $dataGdp   = $searchGdp->search($gdp);
        $dataPcgdp = $searchPcgdp->search($pcgdp);
        $dataPd    = $searchPd->search($pd);
        $dataPcpd  = $searchPcpd->search($pcpd);
        $dataIr    = $searchIr->search($ir);
        $dataUr    = $searchUr->search($ur);
        $dataInr   = $searchInr->search($inr);
        $dataBt    = $searchBt->search($bt);
        $dataFd    = $searchFd->search($fd);

        return $this->render('macro', [
            'model'       => $this->findModel($id),
            'searchGdp'   => $searchGdp,
            'dataGdp'     => $dataGdp,
            'searchPcgdp' => $searchPcgdp,
            'dataPcgdp'   => $dataPcgdp,
            'searchPd'    => $searchPd,
            'dataPd'      => $dataPd,
            'searchPcpd'  => $searchPcpd,
            'dataPcpd'    => $dataPcpd,
            'searchIr'    => $searchIr,
            'dataIr'      => $dataIr,
            'searchUr'    => $searchUr,
            'dataUr'      => $dataUr,
            'searchInr'   => $searchInr,
            'dataInr'     => $dataInr,
            'searchBt'    => $searchBt,
            'dataBt'      => $dataBt,
            'searchFd'    => $searchFd,
            'dataFd'      => $dataFd,
            'meta'        => $meta
        ]);
    }

    /**
     * Displays a single Region model.
     * @param string $id
     * @return mixed
     */
    public function actionMonetaria($id)
    {
        $meta = Metadata::find()
            ->with('category')
            ->where(['category_id' => 3])
            ->asArray()
            ->all();
        
        $searchMs  = new MSSearch();
        $searchMl  = new MLSearch();
        $searchMw  = new MWSearch();
        $searchTrm = new TRMSearch();
        $searchIer = new IERSearch();

        $ms = [
            'MSSearch' => [
                'region_id' => $id
            ] 
        ];
        $ml = [
            'MLSearch' => [
                'region_id' => $id
            ] 
        ];
        $mw = [
            'MWSearch' => [
                'region_id' => $id
            ] 
        ];
        $trm = [
            'TRMSearch' => [
                'region_id' => $id
            ] 
        ];
        $ier = [
            'IERSearch' => [
                'region_id' => $id
            ] 
        ];

        $dataMs  = $searchMs->search($ms);
        $dataMl  = $searchMl->search($ml);
        $dataMw  = $searchMw->search($mw);
        $dataTrm = $searchTrm->search($trm);
        $dataIer = $searchIer->search($ier);

        return $this->render('monetaria', [
            'model'     => $this->findModel($id),
            'searchMs'  => $searchMs,
            'dataMs'    => $dataMs,
            'searchMl'  => $searchMl,
            'dataMl'    => $dataMl,
            'searchMw'  => $searchMw,
            'dataMw'    => $dataMw,
            'searchTrm' => $searchTrm,
            'dataTrm'   => $dataTrm,
            'searchIer' => $searchIer,
            'dataIer'   => $dataIer,
            'meta'      => $meta
        ]);
    }

    /**
     * Displays a single Region model.
     * @param string $id
     * @return mixed
     */
    public function actionFiscal($id)
    {
        $meta = Metadata::find()
            ->with('category')
            ->where(['category_id' => 4])
            ->asArray()
            ->all();
        
        $searchEtrcp = new ETRCPSearch();
        $searchRit   = new RITSearch();
        $searchStr   = new STRSearch();
        $searchRvt   = new RVTSearch();
        
        $etrcp = [
            'ETRCPSearch' => [
                'region_id' => $id
            ] 
        ];
        $rit = [
            'RITSearch' => [
                'region_id' => $id
            ] 
        ];
        $str = [
            'STRSearch' => [
                'region_id' => $id
            ] 
        ];
        $rvt = [
            'RVTSearch' => [
                'region_id' => $id
            ] 
        ];

        $dataEtrcp = $searchEtrcp->search($etrcp);
        $dataRit   = $searchRit->search($rit);
        $dataStr   = $searchStr->search($str);
        $dataRvt   = $searchRvt->search($rvt);

        return $this->render('fiscal', [
            'model'       => $this->findModel($id),
            'searchEtrcp' => $searchEtrcp,
            'dataEtrcp'   => $dataEtrcp,
            'searchRit'   => $searchRit,
            'dataRit'     => $dataRit,
            'searchStr'   => $searchStr,
            'dataStr'     => $dataStr,
            'searchRvt'   => $searchRvt,
            'dataRvt'     => $dataRvt,
            'meta'        => $meta
        ]);
    }

    /**
     * Displays a single Region model.
     * @param string $id
     * @return mixed
     */
    public function actionComercial($id)
    {
        $meta = Metadata::find()
            ->with('category')
            ->where(['category_id' => 5])
            ->asArray()
            ->all();
        
        $searchMta = new MTASearch();
        $searchMdm = new MDMSearch();
        $searchMsm = new MSMSearch();
        $searchFdi = new FDISearch();

        $mta = [
            'MTASearch' => [
                'region_id' => $id
            ] 
        ];
        $mdm = [
            'MDMSearch' => [
                'region_id' => $id
            ] 
        ];
        $msm = [
            'MSMSearch' => [
                'region_id' => $id
            ] 
        ];
        $fdi = [
            'FDISearch' => [
                'region_id' => $id
            ] 
        ];

        $dataMta = $searchMta->search($mta);
        $dataMdm = $searchMdm->search($mdm);
        $dataMsm = $searchMsm->search($msm);
        $dataFdi = $searchFdi->search($fdi);

        return $this->render('comercial', [
            'model'     => $this->findModel($id),
            'searchMta' => $searchMta,
            'dataMta'   => $dataMta,
            'searchMdm' => $searchMdm,
            'dataMdm'   => $dataMdm,
            'searchMsm' => $searchMsm,
            'dataMsm'   => $dataMsm,
            'searchFdi' => $searchFdi,
            'dataFdi'   => $dataFdi,
            'meta'      => $meta
        ]);
    }

    /**
     * Displays a single Region model.
     * @param string $id
     * @return mixed
     */
    public function actionNegocios($id)
    {
        $meta = Metadata::find()
            ->with('category')
            ->where(['category_id' => 6])
            ->asArray()
            ->all();
        
        $searchCci   = new CCISearch();
        $searchIci   = new ICISearch();
        $searchGcr   = new GCRSearch();
        $searchMpfdb = new MPFDBSearch();
        $searchDbi   = new DBISearch();
        $searchGrdi  = new GRDISearch();
      
        $cci = [
            'CCISearch' => [
                'region_id' => $id
            ] 
        ];
        $ici = [
            'ICISearch' => [
                'region_id' => $id
            ] 
        ];
        $gcr = [
            'GCRSearch' => [
                'region_id' => $id
            ] 
        ];
        $mpfdb = [
            'MPFDBSearch' => [
                'region_id' => $id
            ] 
        ];
        $dbi = [
            'DBISearch' => [
                'region_id' => $id
            ] 
        ];
        $grdi = [
            'GRDISearch' => [
                'region_id' => $id
            ] 
        ];

        $dataCci   = $searchCci->search($cci);
        $dataIci   = $searchIci->search($ici);
        $dataGcr   = $searchGcr->search($gcr);
        $dataMpfdb = $searchMpfdb->search($mpfdb);
        $dataDbi   = $searchDbi->search($dbi);
        $dataGrdi  = $searchGrdi->search($grdi);

        return $this->render('negocios', [
            'model'       => $this->findModel($id),
            'searchCci'   => $searchCci,
            'dataCci'     => $dataCci,
            'searchIci'   => $searchIci,
            'dataIci'     => $dataIci,
            'searchMpfdb' => $searchMpfdb,
            'dataMpfdb'   => $dataMpfdb,
            'searchGcr'   => $searchGcr,
            'dataGcr'     => $dataGcr,
            'searchDbi'   => $searchDbi,
            'dataDbi'     => $dataDbi,
            'searchGrdi'  => $searchGrdi,
            'dataGrdi'    => $dataGrdi,
            'meta'        => $meta
        ]);
    }

    /**
     * Displays a single Region model.
     * @param string $id
     * @return mixed
     */
    public function actionSocio($id)
    {
        $meta = Metadata::find()
            ->with('category')
            ->where(['category_id' => 7])
            ->asArray()
            ->all();
        
        $searchHdi = new HDISearch();
        $searchFwi = new FWISearch();
        $searchGpi = new GPISearch();
        $searchWhr = new WHRSearch();
        $searchDi  = new DISearch();
        $searchPci = new PCISearch();
 
        $hdi = [
            'HDISearch' => [
                'region_id' => $id
            ] 
        ];
        $fwi = [
            'FWISearch' => [
                'region_id' => $id
            ] 
        ];
        $gpi = [
            'GPISearch' => [
                'region_id' => $id
            ] 
        ];
        $whr = [
            'WHRSearch' => [
                'region_id' => $id
            ] 
        ];
        $di = [
            'DISearch' => [
                'region_id' => $id
            ] 
        ];
        $pci = [
            'PCISearch' => [
                'region_id' => $id
            ] 
        ];

        $dataHdi = $searchHdi->search($hdi);
        $dataFwi = $searchFwi->search($fwi);
        $dataGpi = $searchGpi->search($gpi);
        $dataWhr = $searchWhr->search($whr);
        $dataDi  = $searchDi->search($di);
        $dataPci = $searchPci->search($pci);

        return $this->render('socio', [
            'model' => $this->findModel($id),
            'searchHdi' => $searchHdi,
            'dataHdi' => $dataHdi,
            'searchFwi' => $searchFwi,
            'dataFwi' => $dataFwi,
            'searchGpi' => $searchGpi,
            'dataGpi' => $dataGpi,
            'searchWhr' => $searchWhr,
            'dataWhr' => $dataWhr,
            'searchDi' => $searchDi,
            'dataDi' => $dataDi,
            'searchPci' => $searchPci,
            'dataPci' => $dataPci,
            'meta' => $meta
        ]);
    }


    /** 
     *  This are the detail view
     */

    /**
     * Displays a single Region model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Displays a single Region model.
     * @param string $id
     * @return mixed
     */
    public function actionStr($id)
    {
        return $this->render('str', [
            'model' => $this->findStr($id),
        ]);
    }

    /**
     * Displays a single Region model.
     * @param string $id
     * @return mixed
     */
    public function actionRit($id)
    {
        return $this->render('rit', [
            'model' => $this->findRit($id),
        ]);
    }

    /**
     * Displays a single Region model.
     * @param string $id
     * @return mixed
     */
    public function actionRvt($id)
    {
        return $this->render('rvt', [
            'model' => $this->findRvt($id),
        ]);
    }

    /**
     * Displays a single Region model.
     * @param string $id
     * @return mixed
     */
    public function actionEtrcp($id)
    {
        return $this->render('etrcp', [
            'model' => $this->findEtrcp($id),
        ]);
    }

    /**
     * Displays a single Region model.
     * @param string $id
     * @return mixed
     */
    public function actionMs($id)
    {
        return $this->render('ms', [
            'model' => $this->findMs($id),
        ]);
    }

    /**
     * Displays a single Region model.
     * @param string $id
     * @return mixed
     */
    public function actionMw($id)
    {
        return $this->render('mw', [
            'model' => $this->findMw($id),
        ]);
    }

    /**
     * Displays a single Region model.
     * @param string $id
     * @return mixed
     */
    public function actionMl($id)
    {
        return $this->render('ml', [
            'model' => $this->findMl($id),
        ]);
    }

    /**
     * Displays a single Region model.
     * @param string $id
     * @return mixed
     */
    public function actionTrm($id)
    {
        return $this->render('trm', [
            'model' => $this->findTrm($id),
        ]);
    }

    /**
     * Displays a single Region model.
     * @param string $id
     * @return mixed
     */
    public function actionIer($id)
    {
        return $this->render('ier', [
            'model' => $this->findIer($id),
        ]);
    }

    /**
     * Displays a single Region model.
     * @param string $id
     * @return mixed
     */
    public function actionFd($id)
    {
        return $this->render('fd', [
            'model' => $this->findFd($id),
        ]);
    }

    /**
     * Displays a single Region model.
     * @param string $id
     * @return mixed
     */
    public function actionGdp($id)
    {
        return $this->render('gdp', [
            'model' => $this->findGdp($id),
        ]);
    }

    /**
     * Displays a single Region model.
     * @param string $id
     * @return mixed
     */
    public function actionPcgdp($id)
    {
        return $this->render('pcgdp', [
            'model' => $this->findPcgdp($id),
        ]);
    }

    /**
     * Displays a single Region model.
     * @param string $id
     * @return mixed
     */
    public function actionPd($id)
    {
        return $this->render('pd', [
            'model' => $this->findPd($id),
        ]);
    }

    /**
     * Displays a single Region model.
     * @param string $id
     * @return mixed
     */
    public function actionPcpd($id)
    {
        return $this->render('pcpd', [
            'model' => $this->findPcpd($id),
        ]);
    }

    /**
     * Displays a single Region model.
     * @param string $id
     * @return mixed
     */
    public function actionIr($id)
    {
        return $this->render('ir', [
            'model' => $this->findIr($id),
        ]);
    }


    /**
     * Displays a single Region model.
     * @param string $id
     * @return mixed
     */
    public function actionUr($id)
    {
        return $this->render('ur', [
            'model' => $this->findUr($id),
        ]);
    }

    /**
     * Displays a single Region model.
     * @param string $id
     * @return mixed
     */
    public function actionInr($id)
    {
        return $this->render('inr', [
            'model' => $this->findInr($id),
        ]);
    }

    /**
     * Displays a single Region model.
     * @param string $id
     * @return mixed
     */
    public function actionBt($id)
    {
        return $this->render('bt', [
            'model' => $this->findBt($id),
        ]);
    }

    /**
     * Displays a single Region model.
     * @param string $id
     * @return mixed
     */
    public function actionMta($id)
    {
        return $this->render('mta', [
            'model' => $this->findMta($id),
        ]);
    }

    /**
     * Displays a single Region model.
     * @param string $id
     * @return mixed
     */
    public function actionMdm($id)
    {
        return $this->render('mdm', [
            'model' => $this->findMdm($id),
        ]);
    }

    /**
     * Displays a single Region model.
     * @param string $id
     * @return mixed
     */
    public function actionMsm($id)
    {
        return $this->render('msm', [
            'model' => $this->findMsm($id),
        ]);
    }

    /**
     * Displays a single Region model.
     * @param string $id
     * @return mixed
     */
    public function actionFdi($id)
    {
        return $this->render('fdi', [
            'model' => $this->findFdi($id),
        ]);
    }

    /**
     * Displays a single Region model.
     * @param string $id
     * @return mixed
     */
    public function actionCci($id)
    {
        return $this->render('cci', [
            'model' => $this->findCci($id),
        ]);
    }

    /**
     * Displays a single Region model.
     * @param string $id
     * @return mixed
     */
    public function actionIci($id)
    {
        return $this->render('ici', [
            'model' => $this->findIci($id),
        ]);
    }

    /**
     * Displays a single Region model.
     * @param string $id
     * @return mixed
     */
    public function actionGcr($id)
    {
        return $this->render('gcr', [
            'model' => $this->findGcr($id),
        ]);
    }

    /**
     * Displays a single Region model.
     * @param string $id
     * @return mixed
     */
    public function actionMpfdb($id)
    {
        return $this->render('mpfdb', [
            'model' => $this->findMpfdb($id),
        ]);
    }

    /**
     * Displays a single Region model.
     * @param string $id
     * @return mixed
     */
    public function actionDbi($id)
    {
        return $this->render('dbi', [
            'model' => $this->findDbi($id),
        ]);
    }

    /**
     * Displays a single Region model.
     * @param string $id
     * @return mixed
     */
    public function actionGrdi($id)
    {
        return $this->render('grdi', [
            'model' => $this->findGrdi($id),
        ]);
    }

    /**
     * Displays a single Region model.
     * @param string $id
     * @return mixed
     */
    public function actionHdi($id)
    {
        return $this->render('hdi', [
            'model' => $this->findHdi($id),
        ]);
    }

    /**
     * Displays a single Region model.
     * @param string $id
     * @return mixed
     */
    public function actionFwi($id)
    {
        return $this->render('fwi', [
            'model' => $this->findFwi($id),
        ]);
    }

    /**
     * Displays a single Region model.
     * @param string $id
     * @return mixed
     */
    public function actionGpi($id)
    {
        return $this->render('gpi', [
            'model' => $this->findGpi($id),
        ]);
    }

    /**
     * Displays a single Region model.
     * @param string $id
     * @return mixed
     */
    public function actionWhr($id)
    {
        return $this->render('whr', [
            'model' => $this->findWhr($id),
        ]);
    }

    /**
     * Displays a single Region model.
     * @param string $id
     * @return mixed
     */
    public function actionDi($id)
    {
        return $this->render('di', [
            'model' => $this->findDi($id),
        ]);
    }

    /**
     * Displays a single Region model.
     * @param string $id
     * @return mixed
     */
    public function actionPci($id)
    {
        return $this->render('pci', [
            'model' => $this->findPci($id),
        ]);
    }

    /**
     * Function to find Models
     */

    /**
     * Finds the Region model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Region the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Region::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Finds the Region model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Region the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findStr($id)
    {
        if (($model = Str::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Finds the Region model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Region the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findRvt($id)
    {
        if (($model = Rvt::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Finds the Region model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Region the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findRit($id)
    {
        if (($model = Rit::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Finds the Region model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Region the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findEtrcp($id)
    {
        if (($model = Etrcp::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Finds the Region model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Region the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findMs($id)
    {
        if (($model = Ms::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Finds the Region model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Region the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findMl($id)
    {
        if (($model = Ml::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Finds the Region model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Region the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findMw($id)
    {
        if (($model = Mw::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Finds the Region model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Region the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findTrm($id)
    {
        if (($model = Trm::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Finds the Region model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Region the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findIer($id)
    {
        if (($model = Ier::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

        /**
     * Finds the Region model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Region the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findGdp($id)
    {
        if (($model = Gdp::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Finds the Region model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Region the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findPcgdp($id)
    {
        if (($model = Pcgdp::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Finds the Region model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Region the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findPd($id)
    {
        if (($model = Pd::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Finds the Region model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Region the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findPcpd($id)
    {
        if (($model = Pcpd::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Finds the Region model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Region the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findIr($id)
    {
        if (($model = Ir::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Finds the Region model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Region the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findUr($id)
    {
        if (($model = Ur::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Finds the Region model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Region the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findInr($id)
    {
        if (($model = Inr::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Finds the Region model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Region the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findBt($id)
    {
        if (($model = Bt::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Finds the Region model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Region the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findFd($id)
    {
        if (($model = Fd::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Finds the Region model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Region the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findMta($id)
    {
        if (($model = Mta::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Finds the Region model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Region the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findMdm($id)
    {
        if (($model = Mdm::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Finds the Region model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Region the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findMsm($id)
    {
        if (($model = Msm::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Finds the Region model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Region the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findFdi($id)
    {
        if (($model = Fdi::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Finds the Region model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Region the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findCci($id)
    {
        if (($model = Cci::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Finds the Region model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Region the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findIci($id)
    {
        if (($model = Ici::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Finds the Region model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Region the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findGcr($id)
    {
        if (($model = Gcr::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Finds the Region model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Region the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findMpfdb($id)
    {
        if (($model = Mpfdb::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Finds the Region model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Region the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findDbi($id)
    {
        if (($model = Dbi::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Finds the Region model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Region the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findGrdi($id)
    {
        if (($model = Grdi::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }



    /**
     * Finds the Region model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Region the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findHdi($id)
    {
        if (($model = Hdi::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Finds the Region model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Region the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findFwi($id)
    {
        if (($model = Fwi::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Finds the Region model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Region the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findGpi($id)
    {
        if (($model = Gpi::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Finds the Region model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Region the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findWhr($id)
    {
        if (($model = Whr::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    /**
     * Finds the Region model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Region the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findDi($id)
    {
        if (($model = Di::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Finds the Region model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Region the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findPci($id)
    {
        if (($model = Pci::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}